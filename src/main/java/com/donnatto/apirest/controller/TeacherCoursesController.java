package com.donnatto.apirest.controller;

import com.donnatto.apirest.model.Course;
import com.donnatto.apirest.model.Teacher;
import com.donnatto.apirest.service.CourseService;
import com.donnatto.apirest.service.TeacherService;
import com.donnatto.apirest.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(Constants.TEACHER_COURSES_URL)
public class TeacherCoursesController {

    private final CourseService courseService;
    private final TeacherService teacherService;

    @Autowired
    public TeacherCoursesController(CourseService courseService, TeacherService teacherService) {
        this.courseService = courseService;
        this.teacherService = teacherService;
    }

    @GetMapping
    public ResponseEntity<List<Course>> getTeacherCourses(@PathVariable Long teacherId) {
        Teacher teacher = teacherService.getTeacher(teacherId);
        if (teacher != null) {
            List<Course> courses = teacher.getCourses();
            return courses != null && !courses.isEmpty() ? ResponseEntity.ok(courses) : new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } else return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @PostMapping
    public ResponseEntity<Void> assignTeacherCourse(@RequestBody Course course, @PathVariable Long teacherId) {
        Teacher teacher = teacherService.getTeacher(teacherId);
        if (teacher != null) {
            List<Course> teacherCourses = teacher.getCourses();
            course.setTeacherId(teacher.getTeacherId());
            Course newCourse = courseService.createCourse(course);
            teacherCourses.add(newCourse);
            teacher.setCourses(teacherCourses);
            teacherService.modifyTeacher(teacher, teacherId);
            return new ResponseEntity<>(HttpStatus.CREATED);
        } else return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

}
