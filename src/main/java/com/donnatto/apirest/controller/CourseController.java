package com.donnatto.apirest.controller;

import com.donnatto.apirest.model.Course;
import com.donnatto.apirest.service.CourseService;
import com.donnatto.apirest.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(Constants.COURSES_URL)
public class CourseController {

    @Autowired
    private CourseService courseService;

    @GetMapping
    public ResponseEntity<List<Course>> getCourses() {
        List<Course> courses = courseService.getAllCourses();
        return courses != null && !courses.isEmpty() ? ResponseEntity.ok(courses) : new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @GetMapping("/{courseId}")
    public ResponseEntity<Course> getCourse(@PathVariable Long courseId) {
        Course course = courseService.getCourse(courseId);
        return course != null ? ResponseEntity.ok(course) : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @PostMapping
    public ResponseEntity<Void> createCourse(@RequestBody Course course) {
        courseService.createCourse(course);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @PutMapping("/{courseId}")
    public ResponseEntity<Void> updateCourse(@RequestBody Course course, @PathVariable Long courseId) {
        courseService.updateCourse(course, courseId);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @PatchMapping("/{courseId}")
    public ResponseEntity<Void> modifyCourse(@RequestBody Course course, @PathVariable Long courseId) {
        courseService.modifyCourse(course, courseId);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @DeleteMapping("/{courseId}")
    public ResponseEntity<Void> deleteCourse(@PathVariable Long courseId) {
        courseService.deleteCourse(courseId);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
