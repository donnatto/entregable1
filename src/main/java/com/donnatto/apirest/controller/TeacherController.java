package com.donnatto.apirest.controller;

import com.donnatto.apirest.model.Teacher;
import com.donnatto.apirest.service.TeacherService;
import com.donnatto.apirest.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(Constants.TEACHERS_URL)
public class TeacherController {

    @Autowired
    private TeacherService teacherService;

    @GetMapping
    public ResponseEntity<List<Teacher>> getTeachers() {
        List<Teacher> teachers = teacherService.getAllTeachers();
        return teachers != null && !teachers.isEmpty() ? ResponseEntity.ok(teachers) : new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @GetMapping("/{teacherId}")
    public ResponseEntity<Teacher> getTeacher(@PathVariable Long teacherId) {
        Teacher teacher = teacherService.getTeacher(teacherId);
        return teacher != null ? ResponseEntity.ok(teacher) : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @PostMapping
    public ResponseEntity<Void> createTeacher(@RequestBody Teacher teacher) {
        teacherService.createTeacher(teacher);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @PutMapping("/{teacherId}")
    public ResponseEntity<Void> updateTeacher(@RequestBody Teacher teacher, @PathVariable Long teacherId) {
        teacherService.updateTeacher(teacher, teacherId);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @PatchMapping("/{teacherId}")
    public ResponseEntity<Void> modifyTeacher(@RequestBody Teacher teacher, @PathVariable Long teacherId) {
        teacherService.modifyTeacher(teacher, teacherId);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @DeleteMapping("/{teacherId}")
    public ResponseEntity<Void> deleteTeacher(@PathVariable Long teacherId) {
        teacherService.deleteTeacher(teacherId);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
