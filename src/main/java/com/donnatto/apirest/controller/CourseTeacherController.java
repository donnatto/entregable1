package com.donnatto.apirest.controller;

import com.donnatto.apirest.model.Course;
import com.donnatto.apirest.model.Teacher;
import com.donnatto.apirest.service.CourseService;
import com.donnatto.apirest.service.TeacherService;
import com.donnatto.apirest.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(Constants.COURSE_TEACHER_URL)
public class CourseTeacherController {

    private final CourseService courseService;
    private final TeacherService teacherService;

    @Autowired
    public CourseTeacherController(CourseService courseService, TeacherService teacherService) {
        this.courseService = courseService;
        this.teacherService = teacherService;
    }

    @GetMapping
    public ResponseEntity<Teacher> getCourseTeacher(@PathVariable Long courseId) {
        Course course = courseService.getCourse(courseId);
        if (course != null) {
            Teacher teacher = teacherService.getTeacher(course.getTeacherId());
            if (teacher != null) return ResponseEntity.ok(teacher);
            else return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } else return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @PostMapping
    public ResponseEntity<Void> assignCourseTeacher(@RequestParam Long teacherId, @PathVariable Long courseId) {
        Course course = courseService.getCourse(courseId);
        Teacher teacher = teacherService.getTeacher(teacherId);
        if (course == null || teacher == null)
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);

        List<Course> courses = teacher.getCourses();
        Course foundCourse = courses.stream().filter(c -> c.getCourseId().equals(courseId)).findAny().orElse(null);
        if (foundCourse == null) {
            courses.add(course);
            teacher.setCourses(courses);
            teacherService.modifyTeacher(teacher, teacherId);
            course.setTeacherId(teacherId);
            courseService.modifyCourse(course, courseId);
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
