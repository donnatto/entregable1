package com.donnatto.apirest.service;

import com.donnatto.apirest.model.Teacher;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

@Service
public class TeacherService {

    private final List<Teacher> teachers = new ArrayList<>();
    private final AtomicLong sequence = new AtomicLong();

    public TeacherService() {
        teachers.add(new Teacher(sequence.incrementAndGet(), "Juan", "Perez", "jperez@gmail.com", 1.5));
        teachers.add(new Teacher(sequence.incrementAndGet(), "Diego", "Lopez", "dlopez@gmail.com", 2.5));
        teachers.add(new Teacher(sequence.incrementAndGet(), "Arturo", "Sanchez", "ashanchez@gmail.com", 3.5));
    }


    public List<Teacher> getAllTeachers() {
        return teachers;
    }

    public Teacher getTeacher(Long teacherId) {
        return teachers.stream().filter(t -> t.getTeacherId().equals(teacherId)).findFirst().orElse(null);
    }


    public void createTeacher(Teacher teacher) {
        long teacherId = sequence.incrementAndGet();
        Teacher newTeacher = new Teacher();
        newTeacher.setTeacherId(teacherId);
        newTeacher.setFirstName(teacher.getFirstName());
        newTeacher.setLastName(teacher.getLastName());
        newTeacher.setEmail(teacher.getEmail());
        newTeacher.setYearsOfExperience(teacher.getYearsOfExperience());

        teachers.add(newTeacher);
    }

    public void updateTeacher(Teacher teacher, Long teacherId) {
        Teacher foundTeacher = findTeacher(teacherId);
        if (teacher.getFirstName() == null || teacher.getFirstName().trim().isEmpty())
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        if (teacher.getLastName() == null || teacher.getLastName().trim().isEmpty())
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        if (teacher.getEmail() == null || teacher.getEmail().trim().isEmpty())
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        if (teacher.getYearsOfExperience() == null || teacher.getYearsOfExperience() < 0)
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);

        Teacher newTeacher = new Teacher();
        newTeacher.setTeacherId(foundTeacher.getTeacherId());
        newTeacher.setFirstName(teacher.getFirstName());
        newTeacher.setLastName(teacher.getLastName());
        newTeacher.setEmail(teacher.getEmail());
        newTeacher.setYearsOfExperience(teacher.getYearsOfExperience());

        teachers.set(teachers.indexOf(foundTeacher), newTeacher);
    }

    public void modifyTeacher(Teacher teacher, Long teacherId) {
        Teacher foundTeacher = findTeacher(teacherId);
        int index = teachers.indexOf(foundTeacher);
        String firstName = teacher.getFirstName();
        String lastName = teacher.getLastName();
        String email = teacher.getEmail();
        Double yearsOfExperience = teacher.getYearsOfExperience();

        if (firstName != null) {
            if (firstName.trim().isEmpty()) throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
            foundTeacher.setFirstName(firstName);
        }
        if (lastName != null) {
            if (lastName.trim().isEmpty()) throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
            foundTeacher.setLastName(lastName);
        }
        if (email != null) {
            if (email.trim().isEmpty()) throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
            foundTeacher.setEmail(email);
        }
        if (yearsOfExperience != null) {
            if (yearsOfExperience < 0) throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
            foundTeacher.setYearsOfExperience(yearsOfExperience);
        }

        teachers.set(index, foundTeacher);
    }

    public void deleteTeacher(Long teacherId) {
        teachers.remove(findTeacher(teacherId));
    }

    private Teacher findTeacher(Long teacherId) {
        Teacher foundTeacher = teachers.stream().filter(t -> t.getTeacherId().equals(teacherId)).findFirst().orElse(null);
        if (foundTeacher == null)
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        return foundTeacher;
    }
}
