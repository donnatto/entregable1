package com.donnatto.apirest.service;

import com.donnatto.apirest.model.Course;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

@Service
public class CourseService {

    private final List<Course> courseList = new ArrayList<>();

    private final AtomicLong sequence = new AtomicLong();

    public CourseService() {
        courseList.add(new Course(sequence.incrementAndGet(), "JS for Beginners", "Curso de JavaScript para principiantes", "Easy"));
        courseList.add(new Course(sequence.incrementAndGet(), "Java for Beginners", "Curso de Java para principiantes", "Easy"));
        courseList.add(new Course(sequence.incrementAndGet(), "Python for Beginners", "Curso de Python para principiantes", "Easy"));
        courseList.add(new Course(sequence.incrementAndGet(), "Intermediate Java", "Curso de Java intermedio", "Intermediate"));
        courseList.add(new Course(sequence.incrementAndGet(), "Advanded Java", "Curso de Java avanzado", "Advanced"));
    }

    public List<Course> getAllCourses() {
        return courseList;
    }

    public Course getCourse(Long id) {
        return courseList.stream().filter(c -> c.getCourseId().equals(id)).findFirst().orElse(null);
    }

    public Course createCourse(Course course) {

        long courseId = sequence.incrementAndGet();
        Course newCourse = new Course();
        newCourse.setCourseId(courseId);
        newCourse.setCourseName(course.getCourseName());
        newCourse.setDescription(course.getDescription());
        newCourse.setDifficulty(course.getDifficulty());
        courseList.add(newCourse);
        return newCourse;
    }

    public void updateCourse(Course course, Long courseId) {
        Course foundCourse = findCourse(courseId);
        if (course.getCourseName() == null || course.getCourseName().trim().isEmpty())
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        if (course.getDescription() == null || course.getDescription().trim().isEmpty())
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        if (course.getDifficulty() == null || course.getDifficulty().trim().isEmpty())
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);

        Course newCourse = new Course();
        newCourse.setCourseId(foundCourse.getCourseId());
        newCourse.setCourseName(course.getCourseName());
        newCourse.setDescription(course.getDescription());
        newCourse.setDifficulty(course.getDifficulty());

        courseList.set(courseList.indexOf(foundCourse), newCourse);
    }

    public void modifyCourse(Course course, Long courseId) {
        Course foundCourse = findCourse(courseId);
        int index = courseList.indexOf(foundCourse);
        String courseName = course.getCourseName();
        String description = course.getDescription();
        String difficulty = course.getDifficulty();

        if (courseName != null) {
            if (courseName.trim().isEmpty()) throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
            foundCourse.setCourseName(courseName);
        }
        if (description != null) {
            if (description.trim().isEmpty()) throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
            foundCourse.setDescription(description);
        }
        if (difficulty != null) {
            if (difficulty.trim().isEmpty()) throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
            foundCourse.setDifficulty(difficulty);
        }

        courseList.set(index, foundCourse);
    }

    public void deleteCourse(Long courseId) {
        Course foundCourse = findCourse(courseId);
        courseList.remove(foundCourse);
    }

    private Course findCourse(Long courseId) {
        Course foundCourse = courseList.stream().filter(c -> c.getCourseId().equals(courseId)).findFirst().orElse(null);
        if (foundCourse == null)
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        return foundCourse;
    }
}
