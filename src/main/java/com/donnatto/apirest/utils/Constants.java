package com.donnatto.apirest.utils;

public class Constants {

    private Constants() {
    }

    public static final String BASE_URL = "/api/v1";
    public static final String COURSES_URL = BASE_URL + "/courses";
    public static final String COURSE_TEACHER_URL = COURSES_URL + "/{courseId}/teacher";
    public static final String TEACHERS_URL = BASE_URL + "/teachers";
    public static final String TEACHER_COURSES_URL = TEACHERS_URL + "/{teacherId}/courses";
}
