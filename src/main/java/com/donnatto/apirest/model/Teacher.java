package com.donnatto.apirest.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Teacher {

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private Long teacherId;
    private String firstName;
    private String lastName;
    private String email;
    private Double yearsOfExperience;
    private List<Course> courses = new ArrayList<>();

    public Teacher(Long teacherId, String firstName, String lastName, String email, Double yearsOfExperience) {
        this.teacherId = teacherId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.yearsOfExperience = yearsOfExperience;
    }
}
