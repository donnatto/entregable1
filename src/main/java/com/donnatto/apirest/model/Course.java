package com.donnatto.apirest.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Course {

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private Long courseId;
    private String courseName;
    private String description;
    private String difficulty;
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private Long teacherId;

    public Course(Long courseId, String courseName, String description, String difficulty) {
        this.courseId = courseId;
        this.courseName = courseName;
        this.description = description;
        this.difficulty = difficulty;
    }
}
